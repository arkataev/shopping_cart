<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 22.09.2016
 */

use Core\Http\Request\Request;
use Core\Router\Router;
use Core\Router\Dispatcher;

require_once '../app/config.php';
require_once '../vendor/autoload.php';
require_once '../app/routes.php';


// Создаем новый запрос
$request = new Request();
// Создаем новую маршрутную таблицу
$router =  new Router();
// Получаем из маршрутной таблицы экземпляр маршрута соответствующий переданному запросу
$route = $router->get_route($request);
//
Dispatcher::dispatch($route, $request);