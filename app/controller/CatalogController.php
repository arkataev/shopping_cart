<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 25.09.2016
 */

namespace App\Controller;

use Core\Controller\Controller;
use Core\Http\Request\Request;


class CatalogController extends Controller
{
	public function index(Request $request)
	{
		$context = ['title' => 'This is a title'];

		return $this->response(['view' => 'catalog'], $context);
	}

	public function get_new(Request $request)
	{
		$context = ['foo' => 'bar'];
		return $this->response()->toJson($context);
	}
}