<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 02.10.2016
 */

namespace App\Controller;


use Core\Controller\Controller;
use Core\Controller\Request;

class CartController extends Controller
{

	public function get_cart(Request $request)
	{
		// Проверить авторизацию пользователя
		// Проверить
		echo 'cart #: ' . $request->get_param('id');
	}

	public function create_cart()
	{

	}

	public function delete()
	{

	}

	public function change_quantity()
	{

	}

	public function calculate_total()
	{

	}
}