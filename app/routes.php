<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 25.09.2016
 */

use Core\Router\Router;


// Добавляем новые маршруты в маршрутную таблицу [путь = Контроллер@Метод]
Router::route(['' => 'CatalogController@index']);
Router::route(['cart/add' => 'CartController@add_cart']);
Router::route(['cart/product/add' => 'CartController@product_add']);
Router::route(['cart/product/remove' => 'CartController@product_remove']);
Router::route(['cart' => 'CartController@get_cart']);

