<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 22.09.2016
 */

namespace Core\Controller;

use ReflectionMethod;
use ReflectionException;
use Core\Http\Response\ResponseFactory;
use Core\Http\Request\Request;


/**
 * Базовый Контроллер
 *
 * Class Controller
 * @package Core\Controller
 */

abstract class Controller
{
	/**
	 * Вызывает метод класса с переданными параметрами с помощью класса ReflectionMethod
	 *
	 * @see http://php.net/manual/ru/class.reflectionmethod.php
	 * @param $method string        Метод класса, который нужно вызывать
	 * @param Request $request      Параметры, которые передаются в вызыванный метод класса
	 *
	 * @return mixed                Возвращает результат работы метода
	 */
	public function execute($method, Request $request)
	{
		try{
			$reflection = new ReflectionMethod($this, $method);
			return $reflection->invoke($this, $request);
		}catch (ReflectionException $e) {
			return false;
		}
	}

	protected function response($response_data=NULL, $context=NULL)
	{
		$response = ResponseFactory::create($response_data);
		$response->set_context($context);

		return $response;
	}
}