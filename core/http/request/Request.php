<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 25.09.2016
 */

namespace Core\Http\Request;


class Request
{
	private $uri;
	private $method;
	private $params;


	public function __construct()
	{
		$this->uri = trim($_SERVER['REQUEST_URI'], '/');
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->params = $this->method == 'POST' ? $_POST : $_GET;
	}

	/**
	 * @return mixed
	 */
	public function get_uri()
	{
		return $this->parse_url_string($this->uri)['path'];
	}

	private function parse_url_string(string $url)
	{
		return parse_url($url);
	}

	public function get_params()
	{
		return $this->params;
	}

	public function get_param(string $key)
	{
		return isset($this->params[$key]) ? $this->params[$key] : false ;
	}

	public function set_param(string $key, $value)
	{
		$this->params[$key] = $value;
	}
}