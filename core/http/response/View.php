<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 02.10.2016
 */

namespace Core\Http\Response;


class View extends Response
{
	private $view;

	public function __construct($view)
	{
		$this->view = $view;
	}

	private function render()
	{
		ob_start();
		extract($this->get_context(), EXTR_OVERWRITE);
		include_once $this->get_template();

		return ob_get_clean();
	}

	public function send()
	{
		// set response-code
		// set response headers
		echo $this->render();
	}

	private function get_template()
	{
		return '../views/' . $this->view . '.php';
	}
}