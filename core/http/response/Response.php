<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 25.09.2016
 */

namespace Core\Http\Response;


abstract class Response
{
	protected $context;
	protected $headers;
	protected $type;


	abstract public function send();



	/**
	 * @return mixed
	 */
	public function get_context()
	{
		return $this->context;
	}

	public function set_context($context)
	{
		$this->context = $context;
	}

	public function setHeader($header)
	{
		return $this;
	}


}