<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 03.10.2016
 */

namespace Core\Http\Response;


abstract class ResponseFactory
{

	private static $response_types = ['view', 'json'];

	public static function create($response_data)
	{
		$type = key($response_data);

		if (in_array($type, self::$response_types)) {
			$response = self::get_response($type, $response_data[$type]);

			return $response;
		}

		return false;
	}

	private static function get_response($type, $data)
	{
		switch ($type){
			case 'view': return new View($data);
		}
	}
}