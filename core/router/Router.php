<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 22.09.2016
 */

namespace Core\Router;

use Core\Http\Request\Request;

/**
 * Класс Маршрутизатора
 * Создает и управляет коллекцией Маршрутов
 *
 * Class Router
 * @package Core\Controller
 */
class Router
{
	/**
	 * Массив добавленных в таблицу маршрутов
	 * @var array
	 */
	private static $routes = [];


	/**
	 * Создает Маршрут и добавляет в общий список
	 *
	 * @param array $route
	 * @return  void
	 */
	public static function route(array $route)
	{
		// путь к контроллеру
		$path = key($route);
		// название контроллера
		$controller = $route[$path];
		// разделяем название контроллера на Контролллер и Метод
		list($controller, $method) = explode('@', $controller);
		// создаем экземпляр Маршрута
		self::$routes[] = new Route($path, $controller, $method);
	}


	/**
	 * Ищет в массиве $routes маршрут, в котором путь совпадает
	 * с путем переданном в Запросе.
	 * Возвращает экземпляр класса Route.
	 *
	 * @param Request $request
	 * @return Route
	 */
	public function get_route(Request $request)
	{
		foreach (self::$routes as $route) {
			if ($route->match($request)) {
				return $route;
			}
		}

		exit('Unable to find route for ' . $request->get_uri());
	}

	public function get_routes()
	{
		return self::$routes;
	}

}