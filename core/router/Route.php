<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 25.09.2016
 */

namespace Core\Router;

use Core\Http\Request\Request;

/**
 * Маршрут совмещает в себе путь к определенному контроллеру и адрес url, который ему
 * соответствует. Маршрут сравнивает адрес из полученного Запроса с адресом
 * своего контроллера, и может возвращать экземпляр класса своего Контролллера
 *
 * Class Route
 * @package Core\Controller
 */
class Route
{
	/**
	 * Route constructor.
	 *
	 * @param $path string          url, которая соответствует контроллеру
	 * @param $controller string    название контроллера
	 * @param $method string        метод контроллера
	 */
	public function __construct($path, $controller, $method)
	{
		$this->path = $path;
		$this->controller = '\\App\\Controller\\'.$controller;
		$this->method = $method;
	}

	/**
	 * Проверяет совпадает ли адрес, указанный в Запросе
	 * с адресом созданного Маршрута
	 *
	 * @param \Core\Controller\Request $request
	 * @return bool
	 */
	public function match(Request $request)
	{
		return $this->path == $request->get_uri();
	}

	/**
	 *
	 * @return string
	 */
	public function get_method()
	{
		return $this->method;
	}

	/**
	 * Возвращает эксземпляр класса Контроллер
	 *
	 * @return Controller       Экземпляр класса Контроллер
	 */
	public function make_controller()
	{
		return new $this->controller;
	}

}