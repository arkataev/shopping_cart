<?php
/**
 * @author RedDev
 * @web https://bitbucket.org/arkataev
 * @date: 25.09.2016
 */

namespace Core\Router;

use Core\Http\Request\Request;
use Core\Router\Route;


/**
 * Диспетчер запросов принимает Путь и Запрос в виде пераметров.
 * Вызывает из Пути нужный Контроллер и передает в него Запрос в виде параметра.
 * Контроллер обрабатывает Запрос и возвращает данные для Ответа в виде массива.
 * Диспетчер создает Ответ и отправляет его пользователю.
 *
 * Class Dispatcher
 * @package Core\Controller
 */
class Dispatcher
{

	public static function dispatch(Route $route, Request $request)
	{
		$controller = $route->make_controller();
		$method = $route->get_method();
		$response  = $controller->execute($method, $request);
		$response->send();
	}

}